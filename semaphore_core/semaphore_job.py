"""Super class of all semaphore_metadata jobs."""

import abc
import os
import shutil
from datetime import date
from typing import Optional

from ifr_boot.ifr_job import Job
from ifr_lib import ifr_files
from ifr_lib.ifr_exception import FileNotFoundException

from semaphore_core.metadata_database import SemaphoreMetadataDB, SemaphoreMetadataIndexEnum


class SemaphoreJob(Job, abc.ABC):
    """SemaphoreJob class

    Attributes:
        metadata_db(SemaphoreMetadataDB): metadata index database object
        tmp_dir(str): temporary directory
    """

    # metadata database object
    metadata_db: SemaphoreMetadataDB = None

    # temporary directory
    tmp_dir: str = None

    def _default_config(self):
        config = super()._default_config()
        config['semaphore'] = {
            'metadata': {
                'archive_path': '${workspace.root_path}/cache/metadata',
                'database_path': '${semaphore.metadata.archive_path}/metadata_index.db',
                'temp_path': '${temp_path}'
            }
        }
        return config

    def _init_context_info(self):
        super()._init_context_info()
        # add metadata path
        self.context_info['metadata_index'] = self.config['semaphore.metadata.database_path']
        self.context_info['metadata_archive'] = self.config['semaphore.metadata.archive_path']

    def _add_config_tokens(self):
        super(Job, self)._add_config_tokens()
        self.config.add_tokens({
            'semaphore': {
                'metadata': {
                    'archive_path': self.config['semaphore.metadata.archive_path']
                }
            }
        })

    def _finalize(self):
        if self.tmp_dir is not None:
            shutil.rmtree(self.tmp_dir, ignore_errors=True)

        super()._finalize()

    def _initialize(self):
        super()._initialize()

        # retrieve and build temp dir
        self.tmp_dir = self.gettempdir(create_dir=True)

        # create metadata archive directory if not exists
        if not os.path.isdir(self.config['semaphore.metadata.archive_path']):
            os.makedirs(self.config['semaphore.metadata.archive_path'])

        # create metadata database object
        self.metadata_db = SemaphoreMetadataDB(self.config['semaphore.metadata.database_path'])

    def get_metadata_path(
            self,
            index_name: SemaphoreMetadataIndexEnum,
            current_date: Optional[date] = date.today(),
            uncompress: bool = False,
            close_database: bool = True
    ) -> str:
        """Retrieve metadata file path

        Args:
            index_name (SemaphoreMetadataIndexEnum): name of the metadata
            current_date (datetime, optional): current date of this treatment
            uncompress (bool): uncompress metadata in temp_path if True
            close_database (bool): close database if True

        Returns:
            The metadata file path

        Raises:
            ItemNotFoundException : if no metadata file is found in database
            FileNotFoundException : if metadata file path does not exists
        """
        relative_path = self.metadata_db.get_metadata_relative_path(index_name, current_date, close_database)

        # build absolute path
        if self.config['semaphore.metadata.archive_path'] is not None:
            absolute_path = os.path.abspath(os.path.join(self.config['semaphore.metadata.archive_path'], relative_path))
        else:
            absolute_path = os.path.abspath(relative_path)

        if not os.path.exists(absolute_path):
            raise FileNotFoundException(f"Metadata file does not exist '{absolute_path}'")

        # return uncompressed file if needed
        if not uncompress:
            return absolute_path

        # uncompress file
        uncompressed_file_path = os.path.join(
            self.config['semaphore.metadata.temp_path'],
            'metadata',
            os.path.basename(ifr_files.strip_extension(relative_path))
        )
        uncompressed_dir_path = ifr_files.parent_dir(uncompressed_file_path)

        if os.path.exists(uncompressed_dir_path):
            if os.path.isfile(uncompressed_file_path):
                return uncompressed_file_path
        else:
            os.makedirs(uncompressed_dir_path)

        ifr_files.gunzip(absolute_path, uncompressed_file_path)

        return uncompressed_file_path
