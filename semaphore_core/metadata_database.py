"""Represents the semaphore_metadata database."""

import enum
import logging
import sqlite3
from datetime import date
from typing import Optional

from ifr_lib.ifr_exception import ItemNotFoundException

from . import SemaphoreCoreException


class SemaphoreMetadataIndexEnum(enum.Enum):
    GEOIP_CITY_INDEX = 'geoip.city'
    GEOIP_ISP_INDEX = 'geoip.isp'
    HACKERS_BLACKLIST_INDEX = 'hackers.blacklist'
    HACKERS_WHITE_LIST_INDEX = 'hackers.whitelist'
    STATIC_COUNTRY_LIST_INDEX = 'static.country'

    CORIOLIS_ARGO_FTP_INDEX = 'coriolis.argo_ftp_index'
    CORIOLIS_PLATFORMS_INDEX = 'coriolis.platforms'
    EDMO_CATALOG_INDEX = 'edmo.catalog'
    JCOMMOPS_PLATFORMS_INDEX = 'jcommops.platforms'
    EDMERP_PROJECTS_INDEX = 'edmerp.projects'

    SEXTANT_CATALOG_INDEX = 'sextant.catalog'
    SEXTANT_SOFTWARE_INDEX = 'sextant.software'

    ODATIS_SOFTWARE_INDEX = 'odatis.software'

    ARCHIMER_CATALOG_INDEX = 'archimer.catalog'
    SEANOE_CATALOG_INDEX = 'seanoe.catalog'


class SemaphoreMetadataDB(object):

    def __init__(self, database_path: str):
        self._log = logging.getLogger(__name__)
        self.database_path = database_path
        self.connection = None

    def connect(self) -> sqlite3.Connection:
        """Connects to the embedded database.
        Creates database and table if not already existing.

        Returns:
            The database connection.
        """
        if self.connection is not None:
            return self.connection

        try:
            self.connection = sqlite3.connect(self.database_path)
            cursor = self.connection.cursor()

            # check if table metadata_index table exists
            cursor.execute("SELECT * FROM sqlite_master WHERE type='table' and tbl_name='metadata_index'")
            table_count = cursor.fetchone()
            if table_count is None:
                self._log.debug('Creating table')
                # create if not
                cursor.execute('''CREATE TABLE IF NOT EXISTS metadata_index(
                            name TEXT NOT NULL,
                            update_date TEXT NOT NULL,
                            file_path TEXT NOT NULL,
                            file_checksum TEXT NOT NULL,
                            PRIMARY KEY (name, update_date, file_path)
                        )''')
            cursor.close()
            return self.connection
        except sqlite3.DatabaseError as error:
            self.connection = None
            raise SemaphoreCoreException(f'A database error occurred : {error.args[0]}')

    def close(self):
        if self.connection:
            self.connection.close()
        self.connection = None

    def retrieve_latest(self, name: str, close_database: bool = True) -> str:
        """Retrieves latest downloaded information from embedded database according to the passed name.

        Args:
            name (str): The metadata name. Example: atlantos.edmo

        Returns:
            The metadata file checksum
        """
        self.connect()
        cursor = None
        try:
            cursor = self.connection.cursor()
            cursor.execute(
                'SELECT update_date, file_checksum '
                'FROM metadata_index WHERE name=? '
                'ORDER BY update_date DESC LIMIT 1',
                (name,)
            )
            return cursor.fetchone()
        except sqlite3.DatabaseError as error:
            raise SemaphoreCoreException(f'A database error occurred : {error.args[0]}')
        finally:
            if cursor is not None:
                cursor.close()
            if close_database:
                self.close()

    def insert_metadata(self, name: str, path: str, checksum: str, close_database: bool = True):
        """Inserts the passed metadata information into the embedded database

        Args:
            name (str): The metadata name. Example: atlantos.edmo
            path (str): The path where the metadata is stored.
            checksum (str): The metadata checksum

        """
        self.connect()
        cursor = None
        file_indexed = False
        try:
            self._log.info('Inserting metadata into metadata_index table')
            cursor = self.connection.cursor()
            cursor.execute(
                'INSERT INTO metadata_index (name, update_date , file_path, file_checksum) '
                "VALUES (?, datetime('now'), ?, ?)",
                (name, path, checksum)
            )
            self.connection.commit()
            self.connection.execute('VACUUM')
            file_indexed = True
        except sqlite3.IntegrityError as error:
            self._log.warning(f'A record already exists with same name, update_date and file_path : {error.args[0]}')
        except sqlite3.DatabaseError as error:
            raise SemaphoreCoreException(f'A database error occurred : {error.args[0]}')
        finally:
            if cursor is not None:
                cursor.close()
            if close_database:
                self.close()
            return file_indexed

    def get_metadata_relative_path(
            self,
            index_name: SemaphoreMetadataIndexEnum,
            current_date: Optional[date] = date.today(),
            close_database: bool = True
    ) -> str:

        self.connect()
        cursor = None

        try:
            cursor = self.connection.cursor()

            # look for the freshest metadata path
            file_path = cursor.execute(
                'SELECT file_path '
                'FROM metadata_index '
                'WHERE name = ? '
                '   AND update_date <= ? '
                'ORDER BY update_date DESC '
                'LIMIT 1 ', (index_name.value, current_date)
            ).fetchone()

            if file_path is not None:
                return file_path[0]

            self._log.warning(f"No metadata file found for index '{index_name.value}' prior to '{current_date}'")
            file_path = cursor.execute(
                    'SELECT file_path '
                    'FROM metadata_index '
                    'WHERE name = ? '
                    '  AND update_date > ? '
                    'ORDER BY update_date ASC '
                    'LIMIT 1 ', (index_name.value, current_date)
                ).fetchone()

            if file_path is not None:
                return file_path[0]

            raise ItemNotFoundException(f"No metadata file found for index '{index_name.value}' prior to '{current_date}'")
        finally:
            if cursor is not None:
                cursor.close()

            if close_database:
                self.close()
