# install conda-lock

```commandline
conda install -c conda-forge conda-lock
```

# generate lock files

```commandline
conda lock --kind explicit -f env-dev.yml -p linux-64 --filename-template 'conda-dev-{platform}.lock'
conda lock --kind explicit -f env-run.yml -p linux-64 --filename-template 'conda-run-{platform}.lock'
```
