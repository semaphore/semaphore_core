# semaphore_core

Semaphore projects core library

## Development

### Install poetry

```bash
pip install poetry poetry-dynamic-versioning poetry2conda
poetry --version
poetry config repositories.nexus-public-release https://nexus-test.ifremer.fr/repository/hosted-pypi-public-release/
```

### retrieve and install project

```bash
git clone https://gitlab.ifremer.fr/semaphore/semaphore_core.git
poetry install -v --no-root
```

### List dependencies

```bash
poetry show --tree
```

# build and publish wheel

```bash
poetry build --format wheel
poetry publish -r nexus-public-release -u nexus-ci -p w2bH2NjgFmQnzVk3
```

# build documentation

```
mkdocs build -f docs/mkdocs.yml
```
