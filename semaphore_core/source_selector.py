"""Semaphore source selector."""

import glob
import os
from datetime import datetime
from typing import List, Optional

from ifr_lib import ifr_datetimes, ifr_files, ifr_str
from ifr_lib.ifr_str import str_format

from semaphore_core.utils import Day, Period


class SourcePath(object):
    def __init__(
        self,
        domain: Optional[str] = None,
        platform: Optional[str] = None,
        bucket: Optional[str] = None,
        site: Optional[str] = None,
    ):
        self.domain = self.format(domain)
        self.platform = self.format(platform)
        self.bucket = self.format(bucket)
        self.site = self.format(site)

    @staticmethod
    def format(value: str) -> str:
        return '*' if value is None else value.strip().lower()[:50]

    def as_path(self, final_path):
        return ifr_str.str_format(final_path, **self.__dict__)


def build_source_paths(
    source_path_pattern: str,
    file_pattern: str,
    start_date: datetime,
    end_date: Optional[datetime] = None,
    sources: Optional[List[SourcePath]] = None,
    period: Period = Day()
):
    """Builds a list of unique sources folder paths.

   Args:
        source_path_pattern (str): The sources path pattern
        file_pattern (str): The file pattern
        start_date (datetime.datetime): The source start date
        end_date (datetime.datetime): The source end date
        sources(list<SourcePath>, optional): The list of sources
        period(Period): period object


   Returns:
        A list of unique sources folder paths

   Examples:

   >>> event_folder_list = build_source_paths(
   >>> sources_path_pattern='/events/events/%Y/DATE=%Y%m%d/SOURCE_DOMAIN={domain}/SOURCE_PLATFORM={platform}/SOURCE_SITE={site}',
   >>> file_pattern='*.json',
   >>> source_date=ifr_datetimes.str_to_datetime('20180425', pattern='%Y%m%d'),
   >>> sources=[SourcePath(domain='www.ifremer.fr', platform='wms', bucket='du', site='biologie'),
   >>>               SourcePath(domain='ftp.ifremer.fr', platform='ftp', bucket='du', site='argo')])
   >>> for event_folder in event_folder_list:
   >>>     print(event_folder)

   "/events/events/2018/DATE=20180425/SOURCE_DOMAIN=www.ifremer.fr/SOURCE_PLATFORM=wms/SOURCE_PROJECT=biologie/SOURCE_SITE=biologie/*.json"
   "/events/events/2018/DATE=20180425/SOURCE_DOMAIN=ftp.ifremer.fr/SOURCE_PLATFORM=ftp/SOURCE_PROJECT=argo/SOURCE_SITE=argo/*.json"

   """
    assert isinstance(source_path_pattern, str)
    assert isinstance(file_pattern, str)
    assert isinstance(start_date, datetime)
    assert end_date is None or isinstance(end_date, datetime)

    def build_path(path_pattern, a_source):
        return ifr_files.to_unix_path(os.path.join(a_source.as_path(path_pattern), file_pattern))

    source_path_pattern = str_format(source_path_pattern, input_period_format=period.format)
    existing_paths = list()
    not_existing_paths = list()

    if end_date is None:
        end_date = start_date

    current_date = start_date
    while current_date <= end_date:
        final_path = ifr_datetimes.datetime_to_str(current_date, source_path_pattern)

        if sources is None:
            existing_paths.append([build_path(final_path, SourcePath())])
        else:
            for source in sources:
                path_builded = build_path(final_path, source)
                if next(glob.iglob(path_builded), None) is not None:
                    existing_paths.append(path_builded)
                else:
                    not_existing_paths.append(path_builded)

        current_date = period.relativedelta(date=current_date, n=1)

    return existing_paths, not_existing_paths
