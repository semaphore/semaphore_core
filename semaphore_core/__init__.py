from ifr_lib.ifr_exception import GenericException


class SemaphoreCoreException(GenericException):
    def __init__(self, *args, **kwargs):
        GenericException.__init__(self, *args, **kwargs)
