import abc
import datetime
import re

from dateutil.relativedelta import relativedelta
from ifr_lib.ifr_exception import PatternException


class Period(abc.ABC):
    format: str

    def normalize_date(self, dt_str: str) -> str:
        return dt_str

    def relativedelta(self, date, n=1) -> datetime.datetime:
        raise NotImplementedError

    def format_date(self, dt: datetime.datetime) -> str:
        return dt.strftime(self.format)

    def get_tree_pattern(self) -> str:
        raise NotImplementedError


class Day(Period):
    format = '%Y%m%d'

    def relativedelta(self, date, n=1) -> datetime.datetime:
        return date + relativedelta(days=n)

    def get_tree_pattern(self) -> str:
        return r'^date=(?P<date>\d{8})$'


class Month(Period):
    format = '%Y%m'

    def normalize_date(self, dt_str: str) -> str:
        return dt_str + '01'

    def relativedelta(self, date, n=1) -> datetime.datetime:
        return date + relativedelta(months=n)

    def get_tree_pattern(self) -> str:
        return r'^date=(?P<date>\d{6})$'


class Year(Period):
    format = '%Y'

    def normalize_date(self, dt_str: str) -> str:
        return dt_str + '0101'

    def relativedelta(self, date, n=1) -> datetime.datetime:
        return date + relativedelta(years=n)

    def get_tree_pattern(self) -> str:
        return r'^date=(?P<date>\d{4})$'


def get_period(dt_str: str) -> Period:
    patterns = {
        'day': r'^[0-9]{8}$',
        'month': r'^[0-9]{6}$',
        'year': r'^[0-9]{4}$',
    }

    for period_unit, period_pattern in patterns.items():
        matcher = re.search(period_pattern, dt_str)
        if matcher is not None:
            if period_unit == 'month':
                return Month()
            if period_unit == 'year':
                return Year()
            return Day()
    raise PatternException(f"Input date is not a valid date '{dt_str}'")
